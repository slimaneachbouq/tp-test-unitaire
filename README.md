# Unit tests - ToDoList

## Installation
1. Clone the repository, build services and Create/Start containers :
```sh
$ git clone https://gitlab.com/slimaneachbouq/tp-test-unitaire.git
$ cd tp-test-unitaire
$ make build
$ make start
```

2. Install dependencies :
```sh
$ make install
```
3. Execute tests (tests will be executed in sqlite memory database) :
```sh
$ make test
```
