########################
## AZULANCE MAKEFILE <3
########################

APP_ENV ?= dev
APP_CONTAINER ?= tptest_app
APP_PROJECT_DIR ?= /var/www/html/

DOCKER ?= docker
EXEC := $(DOCKER) exec -w $(APP_PROJECT_DIR) -u $(id -u ${USER}):$(id -g ${USER}) -ti $(APP_CONTAINER)
EXEC_BASH := $(EXEC) bash -c
EXEC_SSH := $(EXEC) bash

YARN ?= $(EXEC) yarn
COMPOSER ?= $(EXEC) composer
ASSETS_ENV ?= dev

build:
	docker-compose build

start:
	docker-compose up -d

ssh:
	$(EXEC_SSH)

install:
	$(EXEC_BASH) "composer install"

update:
	$(EXEC_BASH) "composer update"

test:
	$(EXEC_BASH) "php ./vendor/bin/phpunit"



