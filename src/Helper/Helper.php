<?php

namespace App\Helper;

use App\Entity\User;
use DateTime;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class Helper
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function isValid(User $user)
    {
        $this->isValidEmail($user->getEmail());

        if (empty($user->getFirstName())) {
            throw new \Exception("First name should not be empty !");
        }

        if (empty($user->getLastName())) {
            throw new \Exception("Last name should not be empty !");
        }

        if(strlen($user->getPassword()) < 8 || strlen($user->getPassword()) > 40) {
            throw new \Exception("Password should be between 8 and 40 characters !");
        }

        if (DateTime::createFromFormat('d-m-Y', $user->getBirthdayString()) === false) {
            throw new \Exception("Wrong date format !");
        }

        if (DateTime::createFromFormat('d-m-Y', $user->getBirthdayString())->diff(new DateTime('NOW'))->y < 13) {
            throw new \Exception("User should have 13 years or older!");
        }

        return True;
    }

    /**
     * @param string $email
     * @throws \Exception
     */
    private function isValidEmail(string $email)
    {
        $emailConstraint = new Assert\Email();
        if ($this->validator->validate($email, $emailConstraint)->count() !== 0) {
            throw new \Exception("Invalid email address !");
        }
    }
}
