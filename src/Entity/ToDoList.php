<?php

namespace App\Entity;

use App\Exception\ItemContentContainsALotOfCharactersException;
use App\Exception\ItemWithTheSameNameAlreadyExistsException;
use App\Exception\MoreThen10ItemsInToDoListException;
use App\Exception\RespectPeriodOf30MinsException;
use App\Repository\ToDoListRepository;
use Carbon\Carbon;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ToDoListRepository::class)
 */
class ToDoList
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="toDoList", cascade={"persist", "remove"})
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity=Item::class, mappedBy="toDoList", cascade={"persist"})
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            if (count($this->items) >= 10) {
                throw new MoreThen10ItemsInToDoListException("You can't create more then 10 items in the same ToDoList !");
            }

            $this->items->filter(function($collectionItem) use ($item) {
                if ($item->getName() === $collectionItem->getName()) {
                    throw new ItemWithTheSameNameAlreadyExistsException("Item with the same name already exists in this ToDoList !");
                }
                return $collectionItem;
            });

            if (!empty($item->getContent()) && strlen($item->getContent()) > 1000) {
                throw new ItemContentContainsALotOfCharactersException("Item content should have less then 1000 characters !");
            }

            /** @var Item $lastItem */
            $lastItem = $this->items->last();
            if ($lastItem) {
                $lastItemCreatedAt = new Carbon($lastItem->getCreatedAt());
                if ($lastItemCreatedAt->diffInMinutes(Carbon::now()) < 30) {
                    throw new RespectPeriodOf30MinsException("You should wait 30 min before create another item !");
                }
            }

            $this->items[] = $item;
            $item->setToDoList($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getToDoList() === $this) {
                $item->setToDoList(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return (string) $this->getId();
    }

}
