<?php

namespace App\EventListener;

use App\Entity\ToDoList;
use App\Service\EmailSenderService;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class ToDoListListener
{
    /**
     * @var EmailSenderService
     */
    private $emailSenderService;

    public function __construct(EmailSenderService $emailSenderService)
    {
        $this->emailSenderService = $emailSenderService;
    }

    public function __invoke(ToDoList $toDoList, LifecycleEventArgs $args)
    {
        if ($toDoList->getItems()->count() === 8) {
            $this->emailSenderService->send();
        }
    }
}
