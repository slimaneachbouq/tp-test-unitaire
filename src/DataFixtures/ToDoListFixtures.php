<?php

namespace App\DataFixtures;

use App\Entity\Item;
use App\Entity\ToDoList;
use App\Entity\User;
use Carbon\Carbon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ToDoListFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $user = new User(
            "userforfixtures1@gmail.com",
            "N6EQa7fmyHETRtkJoMzUw6x7zeHzvrRB9X4nQYh",
            "Jhon",
            "Doe",
            Carbon::now()->subYears(15)->format('d-m-Y')
        );

        $toDoList = new ToDoList();
        $toDoList->setOwner($user);
        for ($itemNbr = 0; $itemNbr < 10; $itemNbr++) {
            $item = new Item();
            $item->setName('item '.$itemNbr);
            $item->setContent('Lorem ipsum');
            $item->setCreatedAt(\DateTimeImmutable::createFromMutable(Carbon::now()->subDays(10 - $itemNbr)));
            $toDoList->addItem($item);
        }

        $user = new User(
            "userforfixtures2@gmail.com",
            "N6EQa7fmyHETRtkJoMzUw6x7zeHzvrRB9X4nQYh",
            "Lorem",
            "Ipsum",
            Carbon::now()->subYears(15)->format('d-m-Y')
        );

        $toDoList2 = new ToDoList();
        $toDoList2->setOwner($user);

        $manager->persist($toDoList);
        $manager->persist($toDoList2);
        $manager->flush();
    }
}
