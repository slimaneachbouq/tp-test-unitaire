<?php

namespace App\Tests\unit;

use App\Entity\User;
use App\Helper\Helper;
use Carbon\Carbon;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validation;

class UserTest extends KernelTestCase{

    private $helper;

    protected function setUp(): void
    {
        $validator    = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $this->helper = new Helper($validator);
    }

    public function testCreatingTwoAccountsWithTheSameEmail()
    {
        $this->expectException(UniqueConstraintViolationException::class);

        $kernel = self::bootKernel();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $userOne = new User(
            "toto@gmail.com",
            "N6EQa7fmyHETRtkJoMzUw6x7zeHzvrRB9X4nQYh",
            "Jhon",
            "Doe",
            Carbon::now()->subYears(15)->format('d-m-Y')
        );
        $userTwo = new User(
            "toto@gmail.com",
            "MzUw6x7zeHzvrRB9X4nQYhMzUw6x7zeHzvrHzvYh",
            "Toto",
            "None",
            Carbon::now()->subYears(15)->format('d-m-Y')
        );

        $entityManager->persist($userOne);
        $entityManager->persist($userTwo);
        $entityManager->flush();
    }

    public function testInvalidMail(){
        $this->expectException('Exception');
        $this->expectExceptionMessage("Invalid email address !");
        $user = new User("@toto.123","password","Jhon","Doe",Carbon::now()->subYears(15)->format('d-m-Y'));
        $this->helper->isValid($user);
    }

    public function testEmptyFirstName(){
        $this->expectException('Exception');
        $this->expectExceptionMessage("First name should not be empty !");
        $user = new User("toto@gmail.com","password","","Doe",Carbon::now()->subYears(15)->format('d-m-Y'));
        $this->helper->isValid($user);
    }

    public function testEmptyLastName(){
        $this->expectException('Exception');
        $this->expectExceptionMessage("Last name should not be empty !");
        $user = new User("toto@gmail.com","password","Jhon","",Carbon::now()->subYears(15)->format('d-m-Y'));
        $this->helper->isValid($user);
    }

    public function testInvalidPassword(){
        $this->expectException('Exception');
        $this->expectExceptionMessage("Password should be between 8 and 40 characters !");
        $user = new User("toto@gmail.com","pass","Jhon","Doe",Carbon::now()->subYears(15)->format('d-m-Y'));
        $this->helper->isValid($user);
    }

    public function testInvalidFormatDateOfBirth(){
        $this->expectException('Exception');
        $this->expectExceptionMessage("Wrong date format !");
        $user = new User("toto@gmail.com","password","Jhon","Doe",Carbon::now()->subYears(15)->format('d/m/Y'));
        $this->helper->isValid($user);
    }

    public function testInvalidAge(){
        $this->expectException('Exception');
        $this->expectExceptionMessage("User should have 13 years or older!");
        $user = new User("toto@gmail.com","password","Jhon","Doe",Carbon::now()->subYears(12)->format('d-m-Y'));
        $this->helper->isValid($user);
    }

    public function testCreatingValidUser()
    {
        $user = new User(
            "toto@gmail.com",
            "N6EQa7fmyHETRtkJoMzUw6x7zeHzvrRB9X4nQYh",
            "Jhon",
            "Doe",
            Carbon::now()->subYears(15)->format('d-m-Y')
        );
        $this->assertTrue($this->helper->isValid($user));
    }
}
