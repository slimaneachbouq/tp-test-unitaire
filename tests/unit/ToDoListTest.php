<?php

namespace App\Tests\unit;

use App\Entity\ToDoList;
use App\Entity\User;
use Carbon\Carbon;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ToDoListTest extends KernelTestCase
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testUserCreateTwoToDdLists(): void
    {
        $this->expectException(UniqueConstraintViolationException::class);

        $user = new User(
            "Jhon@gmail.com",
            "N6EQa7fmyHETRtkJoMzUw6x7zeHzvrRB9X4nQYh",
            "Jhon",
            "Doe",
            Carbon::now()->subYears(15)->format('d-m-Y')
        );
        $user->setToDoList(new ToDoList());
        $this->entityManager->persist($user);

        $toDoList = new ToDoList();
        $toDoList->setOwner($user);

        $this->entityManager->persist($toDoList);
        $this->entityManager->flush();
    }

    public function testUserCreateOneToDdList(): void
    {
        $user = new User(
            "JhonWithToDo@gmail.com",
            "N6EQa7fmyHETRtkJoMzUw6x7zeHzvrRB9X4nQYh",
            "Jhon",
            "Doe",
            Carbon::now()->subYears(15)->format('d-m-Y')
        );
        $this->entityManager->persist($user);

        $toDoList = new ToDoList();
        $toDoList->setOwner($user);

        $this->entityManager->persist($toDoList);
        $this->entityManager->flush();

        /** @var User $createdUserWithToDoList */
        $createdUserWithToDoList = $this->entityManager->getRepository(User::class)->findOneBy(['email' => 'JhonWithToDo@gmail.com']);
        $this->assertNotNull($createdUserWithToDoList->getToDoList());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $schemaTool = new SchemaTool($this->entityManager);
        $metaData = $this->entityManager->getMetadataFactory()->getAllMetadata();
        $schemaTool->dropDatabase();
        $schemaTool->updateSchema($metaData);

        $this->entityManager->close();
        $this->entityManager = null;
    }
}
