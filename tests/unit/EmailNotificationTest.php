<?php

namespace App\Tests\unit;

use App\Entity\Item;
use App\Entity\ToDoList;
use App\Entity\User;
use App\Service\EmailSenderService;
use Carbon\Carbon;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EmailNotificationTest extends KernelTestCase
{
    private $entityManager;

    public function testSendEmailExecutedOnceWhenCreating8Items(): void
    {
        $container = self::$kernel->getContainer();
        $this->entityManager = $container
            ->get('doctrine')
            ->getManager();

        $sendEmail = $this->createMock(EmailSenderService::class);
        $sendEmail->expects($this->once())->method('send')->willReturn(true);

        static::$container->set('App\Service\EmailSenderService', $sendEmail);

        $user = new User(
            "emailnotification@gmail.com",
            "N6EQa7fmyHETRtkJoMzUw6x7zeHzvrRB9X4nQYh",
            "Jhon",
            "Doe",
            Carbon::now()->subYears(15)->format('d-m-Y')
        );

        $toDoList = new ToDoList();
        $toDoList->setOwner($user);
        for ($itemNbr = 0; $itemNbr < 8; $itemNbr++) {
            $item = new Item();
            $item->setName('item '.$itemNbr);
            $item->setContent('Lorem ipsum');
            $item->setCreatedAt(\DateTimeImmutable::createFromMutable(Carbon::now()->subDays(10 - $itemNbr)));
            $toDoList->addItem($item);
            $this->entityManager->persist($toDoList);
        }

        $this->entityManager->flush();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $schemaTool = new SchemaTool($this->entityManager);
        $metaData = $this->entityManager->getMetadataFactory()->getAllMetadata();
        $schemaTool->dropDatabase();
        $schemaTool->updateSchema($metaData);
    }
}
