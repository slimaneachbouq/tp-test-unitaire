<?php

namespace App\Tests;

use App\DataFixtures\ToDoListFixtures;
use App\Entity\Item;
use App\Entity\ToDoList;
use App\Exception\ItemContentContainsALotOfCharactersException;
use App\Exception\ItemWithTheSameNameAlreadyExistsException;
use App\Exception\MoreThen10ItemsInToDoListException;
use App\Exception\RespectPeriodOf30MinsException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ItemTest extends WebTestCase
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $toDoListFixtures = new ToDoListFixtures();
        $toDoListFixtures->load($this->entityManager);
    }

    public function testCreateMoreThen10ItemsInTheSameToDoList(): void
    {
        $this->expectException(MoreThen10ItemsInToDoListException::class);
        $this->expectExceptionMessage("You can't create more then 10 items in the same ToDoList !");

        $item = new Item();
        $item->setName('item for test 1');
        $item->setContent('Lorem ipsum');

        /** @var ToDoList $toDoList Load the toDoList created by fixtures that contains 10 items */
        $toDoList = $this->entityManager->getRepository(ToDoList::class)->find(1);
        $toDoList->addItem($item);

        $this->entityManager->persist($toDoList);
        $this->entityManager->flush();
    }

    public function testCreateTwoItemsWithTheSameNameInTheSameToDoList(): void
    {
        $this->expectException(ItemWithTheSameNameAlreadyExistsException::class);
        $this->expectExceptionMessage("Item with the same name already exists in this ToDoList !");

        $item = new Item();
        $item->setName('item for test 2');
        $item->setContent('Lorem ipsum');

        $item2 = new Item();
        $item2->setName('item for test 2');
        $item2->setContent('Lorem ipsum');

        /** @var ToDoList $toDoList Load the toDoList created by fixtures that does not contain items */
        $toDoList = $this->entityManager->getRepository(ToDoList::class)->find(2);
        $toDoList->addItem($item);
        $toDoList->addItem($item2);

        $this->entityManager->persist($toDoList);
        $this->entityManager->flush();
    }

    public function testCreateItemContentWithMoreThen1000Characters(): void
    {
        $this->expectException(ItemContentContainsALotOfCharactersException::class);
        $this->expectExceptionMessage("Item content should have less then 1000 characters !");

        $item = new Item();
        $item->setName('item for test 3');
        $item->setContent('t9atSafLryWGxgpssxy58XGhe03dkyK1o1HNAbKJeDyEcz3FaT0R0hgzoB7jXAxLe7m1zl8bm6hUr0UPRm
        JyJsNKkpS1GXDEgFkVXAxB8XYwHyaEEchbOXrjGtMZMJMr1SpVGB6Yk3VG5xoJWWTCHUltWZJdKGsytdQORjUFZ8FTarhRuFT059DROK
        9NuitCClsMGaYkfpExl7YxgXudQxqtTBaP66lG4KhIqR4sAJtUe4vK6DkMWyaHR2BbxOiTD5702mdtVUV9syEGyqVab1G0KALrUT8U5Zwx2
        FfAOxiiAzOFmWq9ZT33CxgqmR89VulGqZKRnd93vr2MdDzLEUqVAELg3sYr3hE7aR3Mo7xEqcSYxx9KbKEoIHQxLuLElbSG53h0F8zOhY8
        grwRP8R4UwNOD2VcE4seKlMIIkj3QehbgUIM2JGu482mHeB1CMZvbHSSnq7L59xfXmxBALok4ZEbbAgW1Eaxx30ymcnhls5A4rC2vIyicZWl
        uRYIXpThHXSzSOx8GnWshl9jD7NYDCeog43mDTzlWVwAKkymAX0QRC66EOqQK8Jm5TcWKjX4OYhulaVq3DkE5THpu0Kr5QebCHbpgMrsfAA
        h8nIbXS1ACjm6XBIlxRlqF724HkjJsNzAO2Q5YBgtYuaGmfpWWOTu1qn6uHSD6cCCvok4htyvqkdo3tJDUrCwiH51GZKpbq858bZqxl88Uh
        DBWI1jU4yTK3zCORZ96cbmTJ9H0FFulxWkI2j1ps24XE5baq96aTTNt1g4AeCdUBLSL1FxNWdCT2uJDILq8wImkrYHfCuD1dmnSEcmR7yYR98V
        Vo7LsqzGSm9G3XkuOiwUN27fVp07Id5qlSXMLPd35RqSWQs4w543mnfosZOTnT1w7afxTn6lF6QQP4Op9X94gEhGEo3pYhKXSY0N3c5J6SLnd2a
        0gROFEW89EnYddCvh7UG3OS8Z11kQb5YKszqxtUYdzcb74JFn5g4sN8Yg9q');

        /** @var ToDoList $toDoList Load the toDoList created by fixtures that does not contain items */
        $toDoList = $this->entityManager->getRepository(ToDoList::class)->find(2);
        $toDoList->addItem($item);

        $this->entityManager->persist($toDoList);
        $this->entityManager->flush();
    }

    public function testCreateTwoItemsAtOnceWithoutRespecting30MinRule(): void
    {
        $this->expectException(RespectPeriodOf30MinsException::class);
        $this->expectExceptionMessage("You should wait 30 min before create another item !");

        $item = new Item();
        $item->setName('item for test 4 - one');
        $item->setContent('lorem ipsum');

        /** @var ToDoList $toDoList Load the toDoList created by fixtures that does not contain items */
        $toDoList = $this->entityManager->getRepository(ToDoList::class)->find(2);
        $toDoList->addItem($item);

        $this->entityManager->persist($toDoList);
        $this->entityManager->flush();

        $item2 = new Item();
        $item2->setName('item for test 4 - two');
        $item2->setContent('lorem ipsum');
        $toDoList->addItem($item2);

        $this->entityManager->persist($toDoList);
        $this->entityManager->flush();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $schemaTool = new SchemaTool($this->entityManager);
        $metaData = $this->entityManager->getMetadataFactory()->getAllMetadata();
        $schemaTool->dropDatabase();
        $schemaTool->updateSchema($metaData);

        $this->entityManager->close();
        $this->entityManager = null;
    }
}
