<?php

namespace App\Tests\integration;

use Carbon\Carbon;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ApiUserTest extends WebTestCase
{
    public function testCreateUserWithMissingParameter(): void
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/register',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                "firstName" => "Jhon",
                "lastName" => "Doe",
                "email" => "Jhon@gmail.com",
                "password" => "password",
            ])
        );

        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame('Invalid arguments !', $data['errors']);
        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
    }

    public function testCreateUserWithEmptyFirstName(): void
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/register',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                "firstName" => "",
                "lastName" => "Doe",
                "birthday" => Carbon::now()->format('d-m-Y'),
                "email" => "Jhon@gmail.com",
                "password" => "password",
            ])
        );

        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame('First name should not be empty !', $data['errors']);
        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
    }

    public function testCreateUserWithEmptyLastName(): void
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/register',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                "firstName" => "Jhon",
                "lastName" => "",
                "birthday" => Carbon::now()->format('d-m-Y'),
                "email" => "Jhon@gmail.com",
                "password" => "password",
            ])
        );

        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame('Last name should not be empty !', $data['errors']);
        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
    }

    public function testCreateUserWithEmptyPassword(): void
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/register',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                "firstName" => "Jhon",
                "lastName" => "Doe",
                "birthday" => Carbon::now()->format('d-m-Y'),
                "email" => "Jhon@gmail.com",
                "password" => "",
            ])
        );

        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame('Password should be between 8 and 40 characters !', $data['errors']);
        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
    }

    public function testCreateUserWithPasswordThatHasMoreThen40Characters(): void
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/register',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                "firstName" => "Jhon",
                "lastName" => "Doe",
                "birthday" => Carbon::now()->format('d-m-Y'),
                "email" => "Jhon@gmail.com",
                "password" => "N6EQa7fmyHETRtkJoMzUw6x7zeHzvrRB9X4nQYh99",
            ])
        );

        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame('Password should be between 8 and 40 characters !', $data['errors']);
        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
    }

    public function testCreateUserWithPasswordThatHasLessThen8Characters(): void
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/register',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                "firstName" => "Jhon",
                "lastName" => "Doe",
                "birthday" => Carbon::now()->format('d-m-Y'),
                "email" => "Jhon@gmail.com",
                "password" => "jcxgNhb",
            ])
        );

        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame('Password should be between 8 and 40 characters !', $data['errors']);
        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
    }

    public function testCreateUserWithInvalidEmail(): void
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/register',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                "firstName" => "Jhon",
                "lastName" => "Doe",
                "birthday" => Carbon::now()->format('d-m-Y'),
                "email" => "Jhon@gmail",
                "password" => "N6EQa7fmyHETRtkJoMzUw6x7zeHzvrRB9X4nQYh",
            ])
        );

        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame('Invalid email address !', $data['errors']);
        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
    }

    public function testCreateUserWithEmptyEmail(): void
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/register',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                "firstName" => "Jhon",
                "lastName" => "Doe",
                "birthday" => Carbon::now()->format('d-m-Y'),
                "email" => "Jhon@gmail",
                "password" => "N6EQa7fmyHETRtkJoMzUw6x7zeHzvrRB9X4nQYh",
            ])
        );

        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame('Invalid email address !', $data['errors']);
        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
    }

    public function testCreateUserWithInvalidBirthdayFormat(): void
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/register',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                "firstName" => "Jhon",
                "lastName" => "Doe",
                "birthday" => Carbon::now()->format('d/m/Y'),
                "email" => "Jhon@gmail.com",
                "password" => "N6EQa7fmyHETRtkJoMzUw6x7zeHzvrRB9X4nQYh",
            ])
        );

        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame('Wrong date format !', $data['errors']);
        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
    }

    public function testCreateUserThatHasLessThen13YearsOld(): void
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/register',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                "firstName" => "Jhon",
                "lastName" => "Doe",
                "birthday" => Carbon::now()->subYears(12)->format('d-m-Y'),
                "email" => "Jhon@gmail.com",
                "password" => "N6EQa7fmyHETRtkJoMzUw6x7zeHzvrRB9X4nQYh",
            ])
        );

        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame('User should have 13 years or older!', $data['errors']);
        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
    }

    public function testCreateValidUser(): void
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/register',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                "firstName" => "Jhon",
                "lastName" => "Doe",
                "birthday" => Carbon::now()->subYears(15)->format('d-m-Y'),
                "email" => "Jhon@gmail.com",
                "password" => "N6EQa7fmyHETRtkJoMzUw6x7zeHzvrRB9X4nQYh",
            ])
        );

        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame('User Jhon@gmail.com successfully created', $data['success']);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $kernel = self::bootKernel();
        $entityManager = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $metaData = $entityManager->getMetadataFactory()->getAllMetadata();
        $schemaTool = new SchemaTool($entityManager);
        $schemaTool->dropDatabase();
        $schemaTool->updateSchema($metaData);
    }
}
